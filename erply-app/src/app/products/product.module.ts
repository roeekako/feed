import { NgModule } from '@angular/core';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { RouterModule } from '@angular/router';
import { ProductGuardService } from './product-guard.service';
import { ProductService } from './product.service';
import { CartService } from './cart.service';
import { SharedModule } from '../shared/shared.module';
import { UniquePipe } from '../shared/unique.pipe';
import { CartComponent } from './cart/cart.component';


@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'products', component: ProductListComponent },
      { path: 'products/cart', component: CartComponent },
      { path: 'products/:id',
        canActivate: [ProductGuardService],
         component: ProductDetailsComponent }
    ]),
    SharedModule
  ],
  declarations: [
    ProductListComponent,
    ProductDetailsComponent,
    CartComponent,
    UniquePipe
  ],
  providers: [
    ProductGuardService,
    ProductService,
    CartService
  ],
})
export class ProductModule { }
