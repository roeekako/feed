import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ViewEncapsulation } from '@angular/core';
import { IProduct } from '../product';
import { ProductService } from '../product.service';
import { CartService } from '../cart.service';

@Component({
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class ProductDetailsComponent implements OnInit {
  pageTitle = 'Prduct details';
  cartButton: string = "ADD TO CART";
  errorMessage: string;
  product: IProduct; 

  constructor(private _route: ActivatedRoute,
              private _router: Router,
              private _productService: ProductService,
              private _cartService: CartService) { }

  ngOnInit() {
    const param = this._route.snapshot.paramMap.get('id'); 
    if (param) {
      const id = +param;
      this.getProduct(id);
    }    
  }

  getProduct(id: number) {
    this._productService.getProduct(id).subscribe(
      product => this.product = product, 
      error => this.errorMessage = <any>error);
  }

  onBack(): void {
    this._router.navigate(['7products']);
  }

  onChangeCart(): void { 
    if ( this.cartButton == "ADD TO CART" ) {
      this.cartButton = "REMOVE";
      this._cartService.addProduct(this.product);
    } else {
      this.cartButton = "ADD TO CART"; 
      this._cartService.removeProduct(this.product);
    }    
  }

}
