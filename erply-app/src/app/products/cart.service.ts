import { Injectable } from '@angular/core';
import { IProduct } from '../products/product';


@Injectable({
  providedIn: 'root'
})

export class CartService {

  addProduct(product: IProduct): void {
    let cartItems: any[] = [];
    if (!localStorage.cartItems) { 
      cartItems.push(product);
      localStorage.setItem('cartItems', JSON.stringify(cartItems));
  }  else {
    let savedItems = JSON.parse(localStorage.getItem("cartItems"));
      console.log(savedItems);
      savedItems.push(product);
      localStorage.setItem('cartItems', JSON.stringify(savedItems));
    }
  }

  removeProduct(product: IProduct): void {
    let savedItems = JSON.parse(localStorage.getItem("cartItems"));
      savedItems.forEach( (item, index) => {
        if(item.id === product.id) 
          savedItems.splice(index,1);
        localStorage.setItem('cartItems', JSON.stringify(savedItems));
      });
  }

  getProducts(): IProduct[] {
    let savedItems = JSON.parse(localStorage.getItem("cartItems"));
    return savedItems;
  }

}
