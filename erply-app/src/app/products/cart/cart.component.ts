import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IProduct } from '../product';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class CartComponent implements OnInit {
  cartProducts: IProduct[] = [];
  pageTitle = 'Shopping Cart';
  errorMessage: string;

  constructor(private _route: ActivatedRoute,
    private _router: Router,
    private _cartService: CartService) { }

    ngOnInit() {
        this.cartProducts = this._cartService.getProducts();
    }

    onBack(): void {
      this._router.navigate(['/products']);
    }

    onRemoveProduct(product: IProduct): void { 
      this._cartService.removeProduct(product);
      window.location.reload();
    }


}
