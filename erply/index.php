<!DOCTYPE html>
<html>
<head>
	<title>Erply Frontend Challenge</title>
	<link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bower_components/datatables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<script type="text/javascript" src="bower_components/jquery/dist/jquery.js"></script>
	<script type="text/javascript" src="bower_components/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>