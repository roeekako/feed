$(document).ready(function () { 
	$.post('process.php', {feed: "getFeedItems"}); 
   	$.getJSON("newsfeed.json", function(result) {
   		$.each(result, function(key, val) {
   			if ( val.image != "" ) {
   				$(".container").append("<div class='list-element col-md-6 col-lg-4 col-xs-12' data-link='" + val.link + "''><img src='"+ val.image +"'><h3>" + val.title + "</h3><p class='word-break'>" + val.description + "</p><span class='author'>" + val.author + "</span><span class='date'>" + val.pubDate + "</span></div>" );
   			} else {
   				$(".container").append("<div class='list-element col-md-6 col-lg-4 col-xs-12' data-link='" + val.link + "''><h3>" + val.title + "</h3><p class='word-break'>" + val.description + "</p><span class='author'>" + val.author + "</span><span class='date'>" + val.pubDate + "</span></div>" );
   			}
   		});
   	});

   	$("body").on("click", ".list-element", function(evt) { 
   		var link = $(this).data("link"); console.log(link);
   		$.post('process.php', {link: link}, function(response) {
   			var obj = $.parseJSON(response);
			console.log($.type(obj));
			$.each(JSON.parse(obj), function(key, val) {
				if (key == "content") {
					console.log(val);
					$(".content-body").html(val);
					$(".close-button").addClass("show");
					$("#popup").show();
					$(".list-element").addClass("grey");
					$(".list-element img").hide();
				}
			}); 
		});
   	});

   	$(".close-button").on("click", function(evt) { console.log("evt: " + evt);
   		$(".content-body").html();
   		$(".close-button").removeClass("show");
   		$("#popup").hide();
   		$(".list-element").removeClass("grey");
   		$(".list-element img").show();
   	});
	

});