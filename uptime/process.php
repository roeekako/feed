<?php
if ( isset($_POST['feed']) ) { 
	$url = "https://flipboard.com/@raimoseero/feed-nii8kd0sz?rss";
	$feed = simplexml_load_file($url) or die("Error: Cannot create object");
	getFeeditems($feed);	
}

if ( isset($_POST['link']) ) { 
	$url = $_POST['link'];
	getArticleData($url);
}

function getFeeditems($feed) {
	$feedItems = array();
	$i = 0;
	foreach ($feed->channel[0]->item as $item) { 
		$feedItems[$i] = array();
		$feedItems[$i]['title'] = (string)$feed->channel->item[$i]->title;
		$feedItems[$i]['link'] = (string)$feed->channel->item[$i]->link;
		$feedItems[$i]['image'] = (string)$feed->channel->item[$i]->children('media', True)->content->attributes();;
		$feedItems[$i]['guid'] = (string)$feed->channel->item[$i]->guid;
		$feedItems[$i]['pubDate'] = substr((string)$feed->channel->item[$i]->pubDate,-25,-13);
		$feedItems[$i]['author'] = (string)$feed->channel->item[$i]->author;
		$feedItems[$i]['description'] = (string)$feed->channel->item[$i]->description;
		//$feedItems[$i]['category'] = (string)$feed->channel->item[$i]->category;
		$i++;
	}

	$json = json_encode($feedItems); 
	$fp = fopen('newsfeed.json', 'w');
	fwrite($fp, $json);
	fclose($fp);
}

function getArticleData($url) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://mercury.postlight.com/parser?url=" . $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_HTTPHEADER,array('X-Api-Key: Oy1zITEzp39Xqf570oHrM4BePG3XS203buTRVHB7', 'Content-Type: application/json'));

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
	    echo 'Error:' . curl_error($ch);
	}
	curl_close ($ch);
	echo json_encode($result, JSON_UNESCAPED_UNICODE);
}

?>