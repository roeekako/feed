<?php error_reporting(0); ?>
<!DOCTYPE html>
<html>
<head>
	<title>parsing rss feed</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.css">
	<script type="text/javascript" src="bower_components/jquery/dist/jquery.js"></script>
	<script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</head>
<body>
	<div class="container container-fluid">
		<button class='btn btn-outline-dark close-button'>Close</button>
		<div id="popup">
			<div class="content-body container container-fluid"></div>
		</div>
	</div>
</body>
</html>