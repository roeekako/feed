<?php
namespace PostgreSQL;

class Occupation {
	public $pdo;

	public function __construct($pdo) {
		$this->pdo = $pdo;
	}

	 public function all() {
        $stmt = $this->pdo->query('SELECT * '
                . 'FROM occupation '
                . 'ORDER BY code');
        $stocks = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $stocks[] = [
                'code' => $row['code'],
                'name' => $row['name']
            ];
        }
        return $stocks;
    }

/*    public function read() {
        $this->pdo->beginTransaction();
        $stmt = $this->pdo->prepare("SELECT application, type "
                . "FROM users "
                . "ORDER BY user_id DESC "
                . "LIMIT 1");
 
        // query blob from the database
        $stmt->execute([]);
 
        $stmt->bindColumn('application', $fileData, \PDO::PARAM_STR);
        $stmt->bindColumn('type', $type, \PDO::PARAM_STR);
        $stmt->fetch(\PDO::FETCH_BOUND);
        $stream = $this->pdo->pgsqlLOBOpen($fileData, 'r');
 
        // output the file
        header("Content-type: " . $type);
        fpassthru($stream);
    }*/

}


?>