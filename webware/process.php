
<?php
require 'classes/Connection.php';
require 'classes/Occupation.php';
require 'classes/Insert.php';
require 'classes/Update.php';
use PostgreSQL\Occupation as Occupation; 
use PostgreSQL\Connection as Connection;
use PostgreSQL\Insert as Insert;
use PostgreSQL\Update as Update;
 
try {
    $pdo = Connection::get()->connect();

    $occupation = new Occupation($pdo);

} catch (\PDOException $e) {
    echo $e->getMessage();
}

if (isset($_POST['filter']) && $_POST['filter'] == 'getSelectOptions') {
	$filter = $_POST['filter']; 
	if ($filter == 'getSelectOptions') {
         // get all occupation data for select
        $occupations = $occupation->all(); 
		echo json_encode($occupations);
	}
} 

if (isset($_POST['filter']) && $_POST['filter'] == 'insert') { 
        $firstName  = $_POST['firstName'];
        $lastName   = $_POST['lastName'];
        $occupation = $_POST['occupation'];
        $fileName   = $_POST['fileName'];
        $type       = $_POST['type'];
        $filePath   = "upload/" . $fileName;

        $insert = new Insert($pdo);
        $id = $insert->insertData($firstName, $lastName, $occupation, $fileName, $type, $filePath);

        echo json_encode($id);
} 

if (isset($_POST['filter']) && $_POST['filter'] == "update") {
     $firstName  = $_POST['firstName'];
     $lastName   = $_POST['lastName'];
     $occupation = $_POST['occupation'];

     $update = new Update($pdo);
     $rows = $update->update($firstName, $lastName, $occupation);

     echo json_encode($rows);

} 

if (isset($_POST['filter']) && $_POST['filter'] == 'emptyFolder') {
    $files = glob('upload/*'); //get all file names
    foreach($files as $file){
        if(is_file($file))
        unlink($file); //delete file
    }
    echo json_encode("Folder empty");
}

if (isset($_POST['filter']) && $_POST['filter'] == "file") {
    $path    = 'upload';
    $files = scandir($path);
    $files = array_diff(scandir($path), array('.', '..'));
    $file;
    foreach ($files as $key => $value) {
        $file = $value;
    }
    echo json_encode($file);
}




